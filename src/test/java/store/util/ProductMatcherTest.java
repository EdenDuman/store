package store.util;

import org.junit.Test;
import store.objects.Product;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProductMatcherTest {
  @Test
  public void matchProduct_matchingSecond() {
    List<Product> productList = new ArrayList<>();
    productList.add(Product.builder().productName("גזר").build());
    productList.add(Product.builder().productName("גזר גמדי").build());

    String productString = "גזר גמדי";

    ProductMatcher productMatcher = new ProductMatcher(productList);

    String productExpected = "גזר גמדי";
    String actual =
        productMatcher
            .matchProduct(productString)
            .orElseGet(() -> Product.builder().build())
            .getProductName();

    assertEquals(productExpected, actual);
  }

  @Test
  public void matchProduct_matchingFirst() {
    List<Product> productList = new ArrayList<>();
    productList.add(Product.builder().productName("גזר גמדי").build());
    productList.add(Product.builder().productName("גזר").build());

    String productString = "גזר גמדי";

    ProductMatcher productMatcher = new ProductMatcher(productList);

    String productExpected = "גזר גמדי";
    String actual =
            productMatcher
                    .matchProduct(productString)
                    .orElseGet(() -> Product.builder().build())
                    .getProductName();

    assertEquals(productExpected, actual);
  }
}
