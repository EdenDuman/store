package store.xlsx;

import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XLSXFileReader {
  public Sheet getSheet(FileInputStream fileInputStream, int rowIndex) throws IOException {
    Workbook workbook = new XSSFWorkbook(fileInputStream);

    return workbook.getSheetAt(rowIndex);
  }
}
