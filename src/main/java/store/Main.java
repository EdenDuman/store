package store;

import store.creators.CatalogueCreator;
import store.creators.OrderCreator;
import store.creators.OrderXLSXCreator;
import store.objects.Product;
import store.objects.casomer.Order;

import java.util.List;

public class Main {
  private static final String CATALOGUE_FILE_NAME = "קטלוג.xlsx";

  public static void main(String[] args) {
    CatalogueCreator catalogueCreator = new CatalogueCreator();
    List<Product> productList = catalogueCreator.getProductListFromFile(CATALOGUE_FILE_NAME);

    OrderCreator orderCreator = new OrderCreator(productList);
    List<Order> orderList = orderCreator.createOrders();

    OrderXLSXCreator orderXLSXCreator = new OrderXLSXCreator(orderList);
    orderXLSXCreator.createOrderXLSXFile();
  }
}
