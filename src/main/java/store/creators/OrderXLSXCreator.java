package store.creators;

import lombok.AllArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import store.objects.Product;
import store.objects.casomer.Order;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
public class OrderXLSXCreator {
  private static final int CHARACTER_WIDTH = 256;

  private final List<Order> orderList;

  private String getFileName() {
    Date date = Calendar.getInstance().getTime();
    SimpleDateFormat DateFor = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    String stringDate = DateFor.format(date);

    return String.format("%s-%s.%s", "הזמנה", stringDate, "xlsx");
  }

  private void SheetSize(Sheet sheet, String productName, String amount) {
    if (sheet.getColumnWidth(0) / CHARACTER_WIDTH < productName.length()) {
      sheet.setColumnWidth(0, productName.length() * CHARACTER_WIDTH);
    }

    amount = amount == null ? "" : amount;
    if (sheet.getColumnWidth(1) / CHARACTER_WIDTH < amount.length()) {
      sheet.setColumnWidth(1, amount.length() * CHARACTER_WIDTH);
    }
  }

  private void createSheet(Sheet sheet, Order order) {
    CellStyle redCellStyle = sheet.getWorkbook().createCellStyle();
    redCellStyle.setFillBackgroundColor(IndexedColors.RED.getIndex());
    redCellStyle.setFillPattern(FillPatternType.FINE_DOTS);

    int rowIndex = 0;
    order.getOrderProducts().sort(Product::compareTo);

    for (Product product : order.getOrderProducts()) {
      Row row = sheet.createRow(rowIndex);
      Cell productNameCell = row.createCell(0);
      productNameCell.setCellValue(product.getProductName());

      if (!product.isInCatalogue()) {
        productNameCell.setCellStyle(redCellStyle);
      }

      Cell productAmountCell = row.createCell(1);
      productAmountCell.setCellValue(product.getAmount());

      this.SheetSize(sheet, product.getProductName(), product.getAmount());
      rowIndex++;
    }
  }

  /**
   * sorry
   */
  public void createOrderXLSXFile() {
    try (Workbook workbook = new XSSFWorkbook()) {
      for (Order order : this.orderList) {
        Sheet sheet = workbook.createSheet(order.getCustomerName());
        this.createSheet(sheet, order);
      }

      String fileName = this.getFileName();
      File file = new File(fileName);

      try {
        if (!file.createNewFile()) {
          throw new IOException("Can't create target .XLSX file, fileName=" + fileName);
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

      try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
        workbook.write(fileOutputStream);
      } catch (IOException e) {
        e.printStackTrace();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
