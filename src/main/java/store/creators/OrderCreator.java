package store.creators;

import lombok.AllArgsConstructor;
import store.objects.Product;
import store.objects.casomer.Order;
import store.util.OrderFormatter;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class OrderCreator {
  private static final String ORDER_FOLDER = "הזמנות";

  private final List<Product> productList;

  private List<File> getOrdersFiles() {
    File orderFolder = new File(ORDER_FOLDER);

    if (!orderFolder.isDirectory()) {
      return Collections.emptyList();
    }

    File[] ordersFiles = orderFolder.listFiles();

    return (ordersFiles == null) ? Collections.emptyList() : Arrays.asList(ordersFiles);
  }

  private List<Product> cloneList() {
      return this.productList.stream().map(product -> product.toBuilder().build()).collect(Collectors.toList());
  }

  public List<Order> createOrders() {
    List<File> orderFileList = this.getOrdersFiles();
    List<Order> orderList = new ArrayList<>();

    for (File file : orderFileList) {
      OrderFormatter orderFormatter = new OrderFormatter(this.cloneList());
      orderList.add(orderFormatter.getOrderFromFile(file));
    }

    return orderList;
  }
}
