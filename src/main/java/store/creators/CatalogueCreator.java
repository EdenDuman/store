package store.creators;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import store.objects.Product;
import store.xlsx.XLSXFileReader;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CatalogueCreator {
  private static final String TITLE_PRODUCT_NAME_SHEET = "שם פריט";

  public List<Product> getProductListFromFile(String fileName) {
    List<Product> productList = new ArrayList<>();
    try (FileInputStream fileInputStream = new FileInputStream(fileName)) {
      DataFormatter dataFormatter = new DataFormatter();
      Sheet sheet = new XLSXFileReader().getSheet(fileInputStream, 0);

      for (int rowIndex = 0; rowIndex < sheet.getLastRowNum() + 1; rowIndex++) {
        Row row = sheet.getRow(rowIndex);
        Cell cell = row.getCell(0);
        String productName = dataFormatter.formatCellValue(cell).trim();

        if (!productName.equals(TITLE_PRODUCT_NAME_SHEET) && !productName.isEmpty()) {
          productList.add(
              Product.builder()
                  .productName(productName)
                  .index(rowIndex)
                  .isInCatalogue(true)
                  .isLocked(false)
                  .build());
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    return productList;
  }
}
