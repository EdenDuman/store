package store.util;

import lombok.AllArgsConstructor;
import store.objects.Product;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
public class ProductMatcher {
  private final List<Product> productList;

  public Optional<Product> matchProduct(String productString) {
    List<Product> sortedMatchingProducts =
        this.productList.stream()
            .filter(
                product ->
                    productString.trim().contains(product.getProductName().trim())
                        && !product.isLocked())
            .sorted(
                (product1, product2) ->
                    product2.getProductName().trim().length()
                        - product1.getProductName().trim().length())
            .collect(Collectors.toList());

    return sortedMatchingProducts.isEmpty()
        ? Optional.empty()
        : Optional.ofNullable(sortedMatchingProducts.get(0));
  }
}
