package store.util;

import store.objects.Product;
import store.objects.casomer.Order;

import java.io.BufferedReader;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

public class OrderFormatter {
  private static final String NOT_FOUND = "לא נמצא";
  private static final String TEXT_FILE = ".txt";

  public OrderFormatter(List<Product> productList) {
    this.productList = productList;
    this.productMatcher = new ProductMatcher(productList);
  }

  private final List<Product> productList;

  private final ProductMatcher productMatcher;

  private void setProductFromOrderList(String orderString) {
    Optional<Product> optionalProduct = this.productMatcher.matchProduct(orderString);

    if (optionalProduct.isPresent()) {
      Product product = optionalProduct.get();
      String amount = orderString.replace(product.getProductName(), "").trim();

      if (amount.isEmpty()) {
        amount = "1";
      }

      product.setAmount(amount);
      product.setLocked(true);
    } else {
      this.productList.add(
          Product.builder()
              .productName(orderString)
              .amount(NOT_FOUND)
              .isInCatalogue(false)
              .isLocked(true)
              .build());
    }
  }

  private void readOrderProducts(File file) {
    Path path = Paths.get(file.getPath());

    try (BufferedReader bufferedReader = Files.newBufferedReader(path)) {

      String orderProductString = bufferedReader.readLine();
      while (orderProductString != null) {
        this.setProductFromOrderList(orderProductString);

        orderProductString = bufferedReader.readLine();
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Order getOrderFromFile(File file) {
    String customerName = file.getName().replace(TEXT_FILE, "");
    this.readOrderProducts(file);

    return Order.builder().customerName(customerName).orderProducts(this.productList).build();
  }
}
