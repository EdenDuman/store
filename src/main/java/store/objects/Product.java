package store.objects;

import lombok.Builder;
import lombok.Data;

@Builder(toBuilder = true)
@Data
public class Product implements Comparable<Product> {
  private String productName;

  private int index;

  private String amount;

  private boolean isInCatalogue;

  private boolean isLocked;

  @Override
  public int compareTo(Product product) {
    int myIndex = this.getIndex();
    int orderProductIndex = product.getIndex();

    if (!this.isInCatalogue()) {
      return 1;
    }

    if (!product.isInCatalogue()) {
      return -1;
    }

    return Integer.compare(myIndex, orderProductIndex);
  }
}
