package store.objects.casomer;

import lombok.Builder;
import lombok.Data;
import store.objects.Product;

import java.util.List;

@Data
@Builder
public class Order {
  private String customerName;
  private List<Product> orderProducts;
}
